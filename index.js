var mongo = require("./MongoConnector.js");
var config = require('./config.json');

mongo.init(config.mongodb, function(){ });

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var portnum = config.apiport;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var allowCrossDomain = function(req, res, next) {
res.header('Access-Control-Allow-Origin', '*');
res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

// intercept OPTIONS method
if ('OPTIONS' == req.method) {
    res.send(200);
} else {
    next();
}
};
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(allowCrossDomain);
var server = app.listen(portnum, function() {
var host = server.address().address;
var port = server.address().port;
    console.log("Content Provider Service listening at http://%s:%s", host, port);
});
app.get('/', function(req, res) {
    res.send('MongoConnector API Accessed repo');
});

applyFunctionality();

function applyFunctionality(){
    app.get('/:collection', function(req,res){
        mongo.getAll(req.params.collection, null, function(objarr){
            res.send(objarr);
        });
    });
    app.post('/:collection', function(req,res){
	var o = req.body;
	console.log(o);
        mongo.insert(req.params.collection, o, function(result){
            res.send(result);
        });
    });
    app.get('/:collection/:id', function(req,res){
        mongo.getAll(req.params.collection, {"_id": req.params.id}, function(result){
           res.send(result); 
        });
    });
    app.delete('/:collection/:id/delete', function(req,res){
        mongo.remove(req.params.collection, {"_id": req.params.id}, function(result){
            res.send(result);
        })
    });
}

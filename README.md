A Mongo REST based client
Written By: Gregory Rebisz

Description:
A REST service that accepts a Mongo database connection string located in config.json
Also with provision of an API port for access (Default is 7504)

Installation Instructions:
1. Download the code
2. Within the projects root directory "npm install"
3. In ./node_modules/bson/ext/index.js
      Change lines 10 and 15 to read:
      bson = require('../browser_build/bson');
4. Run with "node index.js"

var Mongo = require('mongo-gyro');
var mongo = null;

module.exports = {
  init: function(url, callback){
    mongo = new Mongo(url, {});
    callback(true);
  },
  getAll: function(collection, search, callback){
    mongo.find(collection, search, function(err, object) {
        callback(object);
    });
  },
  insert: function(collection, object, callback){
    if(object == null)
    {
      callback(false);
    }
    else{
      mongo.insert(collection, object, null, function(){
        callback(true);
      });
    }
  },
  remove: function(collection, object, callback){
    if(object == null)
    {
      callback(false);
    }
    else {
      mongo.remove(collection, object, null, function(){
        callback(true);
      });
    }
  }
}
